class Persona{

	constructor(){
		this.caracteristicas=new Array();
	}
	setCaracteristicas(caracteristicas){
		this.caracteristicas=caracteristicas;
	}
	getCaracteristicas(){
		return this.caracteristicas;
	}
	getCaracteristica(idx){
		return this.caracteristicas[idx];
	}
	setCaracteristica(idx,valor){
		this.caracteristicas[idx]=valor;
	}
	caracteristicasString(){
		var c="";
		for (var i = 0; i < this.caracteristicas.length; i++) {
			c+=this.caracteristicas[i]+",";
		}
		return c;
	}
}
class Nodo{
	constructor(objeto,idx){
		this.objeto=objeto;
		this.idx=idx;
		this.nodosHijos=new Array();
	}
	setObjeto(objeto){
		this.objeto=objeto;
	}
	getObjeto(){
		return this.objeto;
	}
	setHijos(hijos){
		this.nodosHijos=hijos;
	}
	getHijos(){
		return this.nodosHijos;
	}
	setHijo(idx,objeto,posL){
		this.nodosHijos[idx]=new Nodo(objeto,posL);
	}
	getHijo(idx){
		return this.nodosHijos[idx];
	}
	getIdx(){
		return this.idx;
	}
}

class Arbol{

	instancia(objeto,idxRaiz){
		this.raiz=new Nodo(objeto,idxRaiz);
	}
	getRaiz(){
		return this.raiz;
	}
	setRaiz(raiz){
		this.raiz=raiz;
	}
	creaArbol(poblacion){
		var mitad=0;
		var nodos=[];
		if(poblacion.length>=3){
			if(poblacion.length%2==0){
				mitad=parseInt((poblacion.length)/2)-1;
			}else{
				mitad=parseInt((poblacion.length)/2);
			}
			//console.log(mitad);
			nodos.push(this.creaArbol(poblacion.slice(0, mitad)));
			nodos.push(this.creaArbol(poblacion.slice(mitad+1,poblacion.length)));
			var padre=new Nodo(poblacion[mitad]);
			padre.setHijos(nodos);
			//console.log("setea padre");
		}else{
			console.log(poblacion.length);
			nodos.push(new Nodo(poblacion[0]));
			nodos.push(new Nodo(poblacion[2]));
			console.log(nodos);
			var padre=new Nodo(poblacion[1]);
			padre.setHijos(nodos);
		}
		return padre;

	}

	encontrarPadre(poblacion,idxC){
		for (var i = 0; i < poblacion.length; i++) {
			if(poblacion[i].getCaracteristica(idxC)==0 && poblacion[i].getCaracteristica(idxC).length>1){
				console.log(i);
				return i;
			}
		}
		return null;
	}
	limpiarPoblacion(poblacion,idxC){
		var newPobla=[];
		for (var i = 0; i < poblacion.length; i++) {
			if(poblacion[i].getCaracteristica(idxC)!=null && poblacion[i].getCaracteristica(idxC).length>1){
				console.log("dleaeraa");
				newPobla.push(poblacion[i]);
			}
		}
		return newPobla;
	}
	creaArbolIndice(poblacion,idxC,idxV){ //idxC= es el de la caracteristica en la que está; idxV=la del padre
		var nodos=[];
		for (var i = 0; i < poblacion.length; i++) {
			//console.log("es la poblacion");
			if(poblacion[i].getCaracteristica(idxC)==idxV+1 &&  poblacion[i].getCaracteristica(idxC).length>1){
				console.log(poblacion[i].getCaracteristica(0));
				var nodo= new Nodo(poblacion[i],i);
				nodo.setHijos(this.creaArbolIndice(poblacion,idxC,i));
				nodos.push(nodo);
			}
		}
		return nodos;		
	}
	cRaiz(poblacion,idxC,idxV){
		this.raiz=new Nodo(poblacion[idxV],idxV);
		this.raiz.setHijos(this.creaArbolIndice(poblacion,idxC,idxV));
		return this.raiz;
	}
}
class graficarArbol{

	constructor(){
	    this.nodes_object = [];
	    this.edges_object = [];
		this.ventanaGrafica = null;

	}
	
    bonsai(nodo,id){
        this.nodes_object.push({id: nodo.getIdx(), label:nodo.getObjeto().caracteristicasString()})
        //console.log("ajá chico?"+nodes_object[nodes_object.length-1].id);
        if(nodo.getHijos()!=null){
            //console.log("trasputas");
            for (var i = 0; i < nodo.getHijos().length; i++) {
                if(nodo.getHijos()[i]!=null){
                    var holita=(i+1)+'';
                    console.log(" la dice "+(id+holita)); 
                    console.log(holita);              
                    this.edges_object.push({from:nodo.getIdx(), to:nodo.getHijos()[i].getIdx()});
                    //console.log(edges_object[edges_object.length-1].from+" hacia "+edges_object[edges_object.length-1].to); 
                    this.bonsai(nodo.getHijos()[i],(id+holita));
                }
            }
        }
        
    }
    
 	creador(raiz){
 		this.bonsai(raiz,0);    
	    var nodes = new vis.DataSet(this.nodes_object);
	    var edges = new vis.DataSet(this.edges_object);
	    if(this.ventanaGrafica!==null) this.ventanaGrafica.close();
	    this.ventanaGrafica = window.open('','ventana3','width=750,height=780,resizable=false,top='+ ((screen.height - 780) / 2) + ',left=' + ((screen.width - 800) / 2));
	    this.crearPopup();    
	    var container = this.ventanaGrafica.document.getElementById('visualization');
	    var data = {nodes: nodes,edges: edges};
	    //console.log("vamos a graficarCarajo"+edges_object[3].from);
	    var options = {
	        layout: {
	            hierarchical: {
	                direction: "UD",
	                sortMethod: "directed"
	            }
	        }
	    }
	    var network = new vis.Network(container, data, options);
 	}

    crearPopup(){
	    this.ventanaGrafica.document.write('<html lang="en">');
	    this.ventanaGrafica.document.write('<head>');
	    this.ventanaGrafica.document.write('<meta charset="utf-8">');
	    this.ventanaGrafica.document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
	    this.ventanaGrafica.document.write('<meta name="viewport" content="width=device-width, initial-scale=1">');
	    this.ventanaGrafica.document.write('<meta name="description" content="">');
	    this.ventanaGrafica.document.write('<meta name="author" content="">');
	    this.ventanaGrafica.document.write('<title>Árbol libre</title>');
	    this.ventanaGrafica.document.write('<link href="bootstrap/bootstrap.min.css" rel="stylesheet">');
	    this.ventanaGrafica.document.write('<link href="bootstrap/ie10-viewport-bug-workaround.css" rel="stylesheet">');
	    this.ventanaGrafica.document.write('<link href="bootstrap/cover.css" rel="stylesheet">');
	    this.ventanaGrafica.document.write('</head>');
	    this.ventanaGrafica.document.write('<body>');
	    this.ventanaGrafica.document.write('<div class="site-wrapper">');
	    this.ventanaGrafica.document.write('<div class="site-wrapper-inner">');
	    this.ventanaGrafica.document.write('<div class="cover-container">');
	    this.ventanaGrafica.document.write('<div class="inner cover">');
	    this.ventanaGrafica.document.write('<h1 class="cover-heading">Árbol Libre</h1>');  
	    this.ventanaGrafica.document.write('<div class="inner cover" id="visualization" style="height:550px;-webkit-border-radius:13px;background:white;"></div>');
	    this.ventanaGrafica.document.write('</div>');
	    this.ventanaGrafica.document.write('<div class="mastfoot">');
	    this.ventanaGrafica.document.write('<div class="inner">');
	    this.ventanaGrafica.document.write('<p>Curso Matemáticas discretas <a href="#">Math Logic</a>, by <a href="#">Group Math Logic</a>.</p>');
	    this.ventanaGrafica.document.write('</div></div></div></div></div>');
	    this.ventanaGrafica.document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>');
	    this.ventanaGrafica.document.write('</body>');
	    this.ventanaGrafica.document.write('</html>');
	        
	}
}
class Metodos{
	constructor(){
		this.poblacion=new Array();
		this.numMuestra=0;
	}
	getPoblacion(){
		return this.poblacion;
	}
	setPoblacion(poblacion){
		this.poblacion=poblacion;
	}
	getNumMuestra(){
		return this.numMuestra;
	}
	setNumMuestra(muestra){
		this.numMuestra=muestra;
	}
	mostrarPoblacion(){
		var holita=""
		for (var i = 0; i < this.poblacion.length; i++) {
			holita+=this.poblacion[i].caracteristicasString()+"\n";
		}
		return holita;
	}
	convenienciaSegunCantidad(){
		var muestra=new Array();
		for (var i = 0; i < this.numMuestra; i++) {
			muestra.push(this.poblacion[i]);
		}
		console.log(muestra);
		return muestra;
	}
	convenienciaSegunCaracteristica(idxCarac){
		var muestra=new Array();
		for (var i = 0; i < this.poblacion.length; i++) {
			if(this.poblacion[i].getCaracteristica(idxCarac)!=null && this.poblacion[i].getCaracteristica(idxCarac).length!=1){
				
				muestra.push(this.poblacion[i]);	
			}
			
		}

		return muestra;	
	}
	convenienciaEnUnRango(min){
		var muestra=new Array();
		var max=this.numMuestra+(min-1);
		for (var i = min-1; i < max; i++) {
			muestra.push(this.poblacion[i]);
		}
		return muestra;
	}
	bolaDeNieve(idxSeg){
		var muestra=new Array();
		for (var i = 0; i < this.poblacion.length; i++) {
			if(this.poblacion[i].getCaracteristica(idxCarac)!=null){
				muestra.push(this.poblacion[i]);	
			}
			
		}
		return muestra;	
	}
	encontrarTipo(){
		var muestra=new Array();
		var num=0;
		console.log();
		var cantY=this.poblacion[0].getCaracteristicas().length;
		for (var i = 0; i < this.poblacion.length; i++) {
			for (var j = num; j < cantY; j++) {
				if(this.poblacion[i].getCaracteristica(j)!=null){
					if(parseFloat(this.poblacion[j].getCaracteristica(j))+""!="NaN"){
						console.log(this.poblacion[j].getCaracteristica(i));
						console.log(parseFloat(this.poblacion[i].getCaracteristica())+""=="NaN");
						muestra.push("numerito");
						num++;
					}else{
						muestra.push("letra");
						num++;
					}
				}
			}
			if(num==cantY){break;}
			
		}
		return muestra;	
	}
	casual(){
		var muestra=new Array();
		var tamanoPoblacion=this.poblacion.length;
		var inicio=Math.round(Math.random());
		var saltito=Math.round(tamanoPoblacion/this.numMuestra);
		for (var i = inicio; i < tamanoPoblacion && muestra.length<=this.numMuestra; i=i+saltito) {
			console.log(tamanoPoblacion+","+this.numMuestra+" ,"+i);
			muestra.push(this.poblacion[i]);
		}
		return muestra;	
	}
	porCaractaristicas(caracteristicasM,yolo){
		var muestra=this.poblacion;
		
		for (var j = 0; j < caracteristicasM.length; j+=2) {
			var arrTemp=[];	
			for (var i = 0; i < muestra.length; i++) {
				
				if("<"==yolo[j]){
					if(muestra[i].getCaracteristica(caracteristicasM[j])<caracteristicasM[j+1]){
						arrTemp.push(muestra[i]);	
					}
				}else if(">"==yolo[j]){
					if(muestra[i].getCaracteristica(caracteristicasM[j])>caracteristicasM[j+1]){
						arrTemp.push(muestra[i]);	
					}
				}else{
					if(muestra[i].getCaracteristica(caracteristicasM[j])==caracteristicasM[j+1]){
						arrTemp.push(muestra[i]);	
					}
				}
			}
			
			muestra=arrTemp;
		}
		return arrTemp;

	}
}